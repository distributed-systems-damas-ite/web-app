package com.services.webapp.controllers;

import com.services.webapp.domain.Project;
import com.services.webapp.dto.CreateProjectDTO;
import com.services.webapp.dto.CreateTaskDTO;
import com.services.webapp.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping("projects")
public class ProjectsController {

    @Autowired
    ProjectService projectService;

    @GetMapping
    public String getAll(@RequestParam(required = false) Long userId, Model model) {
        List<Project> projects = projectService.getProjects(userId);
        model.addAttribute("projects", projects);
        model.addAttribute("userId", userId);
        return "projects/index";
    }

    @GetMapping("/create")
    public String createPage(@RequestParam Long userId, Model model) {
        model.addAttribute("userId", userId);
        model.addAttribute("project", new CreateProjectDTO());
        return "projects/create";
    }

    @PostMapping
    public RedirectView create(@RequestParam Long userId, CreateProjectDTO project) {
        Project newProject = projectService.create(project, userId);
        return new RedirectView("/tasks?projectId=" + newProject.getId(), false);
    }

}
