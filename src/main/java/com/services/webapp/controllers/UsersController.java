package com.services.webapp.controllers;

import com.services.webapp.domain.Project;
import com.services.webapp.domain.User;
import com.services.webapp.dto.LoginRequest;
import com.services.webapp.dto.RegisterRequest;
import com.services.webapp.services.ProjectService;
import com.services.webapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping("users")
public class UsersController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;

    @GetMapping("login")
    public String loginPage(Model model) {
        model.addAttribute("user", new LoginRequest());
        return "users/login";
    }

    @PostMapping("login")
    public RedirectView login(LoginRequest request) {
        User user = userService.login(request);
        List<Project> projects = projectService.getProjects(user.getId());
        return new RedirectView("/projects?userId=" + user.getId(), false);
    }

    @GetMapping("register")
    public String registerPage(Model model) {
        model.addAttribute("user", new RegisterRequest());
        return "users/register";
    }

    @PostMapping("register")
    public RedirectView register(RegisterRequest request) {
        User user = userService.register(request);
        System.out.println(user.getId());
        System.out.println(user.getName());
        return new RedirectView("/projects?userId=" + user.getId(), false);
    }
}
