package com.services.webapp.controllers;

import com.services.webapp.domain.Task;
import com.services.webapp.dto.CreateTaskDTO;
import com.services.webapp.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping("tasks")
public class TasksController {

    @Autowired
    TaskService taskService;

    @GetMapping
    public String login(@RequestParam(required = false) Long projectId, Model model) {
        List<Task> tasks = taskService.getTasks(projectId);
        model.addAttribute("tasks", tasks);
        model.addAttribute("projectId", projectId);
        return "tasks/index";
    }

    @GetMapping("/create")
    public String createPage(@RequestParam Long projectId, Model model) {
        model.addAttribute("projectId", projectId);
        CreateTaskDTO task = new CreateTaskDTO();
        task.setProjectId(projectId);
        model.addAttribute("task", task);
        return "tasks/create";
    }

    @PostMapping
    public RedirectView create(CreateTaskDTO taskDTO) {
        Task task = taskService.create(taskDTO);
        return new RedirectView("/tasks?projectId=" + taskDTO.getProjectId(), false);
    }

}
