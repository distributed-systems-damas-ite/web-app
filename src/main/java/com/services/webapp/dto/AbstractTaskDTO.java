package com.services.webapp.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public abstract class AbstractTaskDTO {

    @NotNull(message = "project id is required")
    private Long projectId;

    @NotBlank(message = "task name is required")
    @NotNull(message = "task name is required")
    private String name;

    private String description;

    @NotNull(message = "task user estimation is required")
    private Float userEstimation;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getUserEstimation() {
        return userEstimation;
    }

    public void setUserEstimation(Float userEstimation) {
        this.userEstimation = userEstimation;
    }
}
