package com.services.webapp.domain;

public class Task {

    private Long id;

    private Long projectId;

    private String name;

    private String description;

    private Float userEstimation;

    private Float aiEstimation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getUserEstimation() {
        return userEstimation;
    }

    public void setUserEstimation(Float userEstimation) {
        this.userEstimation = userEstimation;
    }

    public Float getAiEstimation() {
        return aiEstimation;
    }

    public void setAiEstimation(Float aiEstimation) {
        this.aiEstimation = aiEstimation;
    }
}
