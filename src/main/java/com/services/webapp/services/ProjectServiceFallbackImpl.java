package com.services.webapp.services;

import com.services.webapp.domain.Project;
import com.services.webapp.dto.CreateProjectDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Component
public class ProjectServiceFallbackImpl implements ProjectService {

    @Autowired
    CacheManager cacheManager;

    public List<Project> getProjects(Long userId) {
        Cache.ValueWrapper w = cacheManager.getCache("projects").get(userId);
        if (w != null) {
            return (List<Project>) w.get();
        } else {
            return Collections.emptyList();
        }
    }

    public Project create( CreateProjectDTO project, Long userId) {
        Project p = new Project();
        p.setUserId(0L);
        p.setName("New Project");
        p.setDescription("Project description");
        p.setTasks(new ArrayList<>());
        p.setUserId(0L);
        return p;
    }
}
