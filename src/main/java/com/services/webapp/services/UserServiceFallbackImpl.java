package com.services.webapp.services;

import com.services.webapp.domain.User;
import com.services.webapp.dto.LoginRequest;
import com.services.webapp.dto.RegisterRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

@Component
public class UserServiceFallbackImpl implements UserService {

    @Autowired
    CacheManager cacheManager;

    public User register(RegisterRequest request) {
        User user = new User();
        user.setId(1L);
        user.setName("User");
        user.setUsername("username");
        return user;
    }

    public User login(@RequestBody LoginRequest request){
        User user = new User();
        user.setId(1L);
        user.setName("User");
        user.setUsername("username");
        return user;
    }
}
