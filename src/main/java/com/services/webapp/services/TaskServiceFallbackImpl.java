package com.services.webapp.services;

import com.services.webapp.domain.Task;
import com.services.webapp.dto.CreateTaskDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;


@Component
public class TaskServiceFallbackImpl implements TaskService {

    @Autowired
    CacheManager cacheManager;

    public List<Task> getTasks(Long projectId) {
        Cache.ValueWrapper w = cacheManager.getCache("tasks").get(projectId);
        if (w != null) {
            return (List<Task>) w.get();
        } else {
            return Collections.emptyList();
        }
    }

    public Task create(CreateTaskDTO createTaskDTO){
        Task t = new Task();
        t.setId(0L);
        t.setName("New Task");
        t.setDescription("Task description");
        t.setProjectId(0L);
        t.setUserEstimation(0F);
        t.setAiEstimation(0F);
        return t;
    }
}
