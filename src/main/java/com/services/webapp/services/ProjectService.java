package com.services.webapp.services;

import com.services.webapp.domain.Project;
import com.services.webapp.dto.CreateProjectDTO;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "GATEWAY-SERVICE/api/projectservice", fallback = ProjectServiceFallbackImpl.class)
public interface ProjectService {

    @CachePut("projects")
    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    List<Project> getProjects(@RequestParam(required = false) Long userId);

    @RequestMapping(value = "/projects", method = RequestMethod.POST, consumes = "application/json")
    Project create(@RequestBody CreateProjectDTO project, @RequestParam("userId") Long userId);
}
