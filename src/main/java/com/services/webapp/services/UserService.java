package com.services.webapp.services;

import com.services.webapp.domain.User;
import com.services.webapp.dto.LoginRequest;
import com.services.webapp.dto.RegisterRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "GATEWAY-SERVICE/api/userservice/users", fallback = UserServiceFallbackImpl.class)
public interface UserService {
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    User register(@RequestBody  RegisterRequest request);

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    User login(@RequestBody LoginRequest request);
}
