package com.services.webapp.services;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.services.webapp.domain.Task;
import com.services.webapp.dto.CreateTaskDTO;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "GATEWAY-SERVICE/api/taskservice", fallback = TaskServiceFallbackImpl.class)
public interface TaskService {
    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    @CachePut("tasks")
    List<Task> getTasks(@RequestParam(required = false) Long projectId);

    @RequestMapping(value = "/tasks", method = RequestMethod.POST)
    Task create(@RequestBody CreateTaskDTO createTaskDTO);
}
