package com.services.webapp;

import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CacheManagerConfig {
    @Bean
    public CacheManager alternateCacheManager() {
        return new ConcurrentMapCacheManager("projects", "tasks", "users");
    }
}
